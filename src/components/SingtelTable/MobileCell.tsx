import styled from "styled-components";
import React from "react";

export const MobileCell = styled.div`
display: flex;
flex-direction: row;
gap: 1rem;
label{
    font-family: 'AvenirHeavy';
}
`